#Programa 'Representación de ref. bibliograficas'
require "prc9/version"
module Bibliografia
class Bibliografia
	include Comparable
	#Atributos necesarios para la correcta impresion de la bibliografia
	#Recordar los atributos pasados como ref (to_s)
	#Para el autor (1o+),la serie (0o1), y el isbn (1o+) pasarle en el fichero de pruebas un vector

attr_reader :autor, :titulo, :serie, :editorial, :edicion, :fecha ,:ISBN

def initialize(aut,tit,se,edit,edic,fech,isb)
	@autor = aut
	@titulo = tit.to_s
	@serie = se.to_s
	@editorial = edit.to_s
	@edicion = edic.to_s
	@fecha = fech.to_s
	@ISBN = isb
end

	#Metodo para mostrar la bibliografia
		#tipologia:
			#nombre de autores
			#titulo
			#(serie)
			#editorial; x edicion  (fecha)
			#ISBN
def mostrar()
			if ((@autor == " ") and (@titulo == " ") and (@serie == " ") and (@editorial == " ") and (@edicion == " ") and (@fecha == " ") and (@ISBN == " "))
				print "Bibliografia vacia"
			else
	puts @autora
	puts @titulo
		if @serie != " "
			print "(",@serie,")"
			puts
		end
	print @editorial,";",@edicion," edicion  ","(",@fecha,")"
	puts
	puts @ISBN
			end
end

	#Metodo para obtener el autor
def get_autor()
	@autor
end

	#Metodo para obtener el titulo
def get_titulo()
	@titulo
end

	#Metodo para obtener la serie
def get_serie()
	@serie
end

	#Metodo para obtener la editorial
def get_editorial()
	@editorial
end

	#Metodo para obtener la edicion
def get_edicion()
	@edicion
end

	#Metodo para obtener la fecha de edicion
def get_fecha()
	@fecha
end

	#Metodo para obtener ISBN
def get_ISBN()
	@ISBN
end

	#Metodo para formatear la bibliografia e imprimirla
def get_formatref()
	initialize(" "," "," "," "," "," "," ")
	mostrar()
end
def <=>(anOther)
    autor.size <=> anOther.autor.size
  end
end
end