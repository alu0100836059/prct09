module Bibliografia

	require_relative "bibliografia"

	class Articulo_Digital < Bibliografia
		attr_accessor :pub

		def initialize(tp, pp, t, a, url, n, f)
			@tipo_pub = tp.to_s
			@pp = pp.to_s
			@autor = t
			@titulo = a.to_s
			@url = url.to_s
			@num_edicion = n.to_s
			@fecha_publicacion = f.to_s
		end
	end

end