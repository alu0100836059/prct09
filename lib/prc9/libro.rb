module Bibliografia

	require_relative "bibliografia"
	
	class Libro < Bibliografia
		attr_accessor :pub

		def initialize(tp, t, a, e, n, s, f, i)
			@tipo_pub = tp.to_s
			@autor = t
			@titulo = a.to_s
			@editorial = e.to_s
			@num_edicion = n.to_s
			@serie = s.to_s
			@fecha_publicacion = f.to_s
			@isbn = i
		end
	end
end